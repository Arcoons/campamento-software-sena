<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BootcampResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return[
            "name"=> $this->name,
            "description"=>$this->description,
            "website"=>$this->website,
            "phone"=>$this->phone,
            "user_id"=>$this->user_id,
            "avarage_rating"=>$this->avarage_rating,
            "avarage_cost"=>$this->avarage_cost
        ];
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Bootcamp;
use App\Http\Requests\StoreBootcampRequest;
use App\Http\Resources\BootcampCollection;
use App\Http\Resources\BootcampResource;
use App\Http\Controllers\BaseController;
use Illuminate\Database\QueryException;
class BootcampController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
          return $this->sendResponce(new BootcampCollection(Bootcamp::all()));
        }catch(\Exception $e){
            return $this->sendError('Server error', 500);

        }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBootcampRequest $request)
    {  
        $b= new Bootcamps();
        $bootcamp = new Bootcamp();
            $bootcamp->name = $b->name;
            $bootcamp->description =$b->description;
            $bootcamp->website=$b->website;
            $bootcamp->phone=$b->phone;
            $bootcamp->user_id=$b->user_id;
            $bootcamp->avarage_rating=$b->avarage_rating;
            $bootcamp->avarage_cost=$b->avarage_cost;
            $bootcamp->save();
        try{
            return $this->sendResponce(new BootcampResource (Bootcamp::create($request->all())));
        }catch(\Exception $e){
            return $this->sendError('Server error', 500);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            //1.Encontrar el bootcamp por id
        $bootcamp=Bootcamp::find($id);
        //2.En caso de que el bootcampo no exista 
        if(!$bootcamp){
            return $this->sendError("Bootcamp with id:$id not found", 400);
        }
        return $this->sendResponce(new BootcampResource($bootcamp));
        }catch(\Exception $e){
            return $this->sendError('Server error', 500);

        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            //Localizar el bootcamp con id
        $b=Bootcamp::find($id);
        if(!$b){
            return $this->sendError("Bootcamp with id:$id not found", 400);
        }
        //Actualizar el bootcamp
       $b->update($request->all());
       return $this->sendResponce(new BootcampResource($b));
        }catch(\Exception $e){
            return $this->sendError('Server error', 500);

        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $b=Bootcamp::find($id);
            if(!$b){
             return $this->sendError("Bootcamp with id:$id not found", 400);
         }
            $b->delete();
            return $this->sendResponce(new BootcampResource($b));
        }catch(\Exception $e){
            return $this->sendError('Server error', 500);

        }
       
    }
}

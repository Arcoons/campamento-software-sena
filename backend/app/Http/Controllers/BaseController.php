<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BaseController extends Controller
{
    /*
        Responses exitosas
    */
    public function sendResponce($data, $http_status= 200){
        //Contruir la respuesta
        $respuesta= [
            "success"=> true,
            "data"=>$data,
        ];
        //Enviar  response afirmativa al cliente
        return response()->json($respuesta, $http_status);

    }
    /*
        Responses de error
    */
    public function sendError($errores, $http_status= 404){
        //Counstruir la respuesta de error
        $respuesta=[
            "success"=>false,
            "errors"=>$errores
        ];
        // Enviar response de error
        return response()->json($respuesta, $http_status);
    }
}

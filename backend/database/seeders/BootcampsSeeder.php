<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
Use File;
use App\Models\Bootcamp;

class BootcampsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Conectarnos al archivo jason
        $json=File::get("database/_data/bootcamps.json");
        $array_bootcamps=json_decode($json);
        //recorrer el archivo, 
        foreach($array_bootcamps as $b){
            //por cada instancia, crear un bootcamp
            $bootcamp = new Bootcamp();
            $bootcamp->name = $b->name;
            $bootcamp->description =$b->description;
            $bootcamp->website=$b->website;
            $bootcamp->phone=$b->phone;
            $bootcamp->user_id=$b->user_id;
            $bootcamp->avarage_rating=$b->avarage_rating;
            $bootcamp->avarage_cost=$b->avarage_cost;
            $bootcamp->save();
        }
    }
}
